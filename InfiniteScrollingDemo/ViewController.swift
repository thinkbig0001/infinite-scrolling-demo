//
//  ViewController.swift
//  InfiniteScrollingDemo
//
//  Created by TAPAN BISWAS on 7/25/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var model : RedditDataModel = RedditDataModel()
    var cache : NSCache<AnyObject,AnyObject>!
    
    @IBOutlet weak var displayTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set delegate and datasource for tableView
        displayTable.delegate = self
        displayTable.dataSource = self
        displayTable.prefetchDataSource = self
        cache = NSCache()
        
        //Initiate the data load for the table
        loadData()
    }

    private func loadData() {
        //Reload table only after remote dataload completes
        model.loadDataFromRemote { (true) in
            OperationQueue.main.addOperation {
                self.displayTable.reloadData()
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RedditTableCell
        
        cell.title.text = model.records[indexPath.row].title
        cell.imageUrl = model.records[indexPath.row].thumbnail
        cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

//MARK:- Delegates for Prefetching rows
extension ViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        //Trigger next load before reaching end of current dataset (each call returns 25 records)
        for indexPath in indexPaths {
            if indexPath.row < model.records.count  - 10 {
                continue
            } else {
                model.loadDataFromRemote { (true) in
                    OperationQueue.main.addOperation {
                        self.displayTable.reloadData()
                    }
                }
                break
            }
        }
    }
    
}

