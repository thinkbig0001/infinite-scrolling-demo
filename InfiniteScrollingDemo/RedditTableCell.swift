//
//  RedditTableCell.swift
//  InfiniteScrollingDemo
//
//  Created by TAPAN BISWAS on 7/25/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

class RedditTableCell: UITableViewCell {

    var thumbnail: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        label.numberOfLines = 6
        return label
    }()
    
    var imageUrl: String? {
        didSet {
            if let imageUrl = imageUrl {
                thumbnail.loadImageWithUrl(urlString: imageUrl)
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        addSubview(thumbnail)
        addSubview(title)
        
        thumbnail.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        thumbnail.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        thumbnail.heightAnchor.constraint(equalToConstant: 80).isActive = true
        thumbnail.widthAnchor.constraint(equalToConstant: 80).isActive = true

        title.leftAnchor.constraint(equalTo: thumbnail.rightAnchor, constant: 8).isActive = true
        title.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true
        title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
    }
}
