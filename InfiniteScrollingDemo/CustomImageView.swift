//
//  CustomImageView.swift
//  InfiniteScrollingDemo
//
//  Created by TAPAN BISWAS on 7/25/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

//CustomImageView to handle it's own image loading asynchronously
//and save images in cache
class CustomImageView: UIImageView {
    
    private var currentTask: URLSessionTask?
    
    var imageUrlString: String?
    
    func loadImageWithUrl(urlString: String) {
        
        weak var oldTask = currentTask
        currentTask = nil
        oldTask?.cancel()
        
        imageUrlString = urlString
        
        image = nil
        
        //Check for valid URL signature, return default image if not
        if !(imageUrlString?.contains("https://") ?? false) {
            image = UIImage(named: "default")
            return
        }
        
        //If image is available in cache return from cache
        if let cachedImage = ImageCache.shared.getImage(forKey: urlString) {
            image = cachedImage
            return
        }
        
        //Load image from remote URL now
        if let url = URL(string: urlString) {
            let session = URLSession.shared
            let dataTask = session.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print(error)
                    return
                }
                
                if let unwrappedData = data, let downloadedImage = UIImage(data: unwrappedData) {
                    DispatchQueue.main.async(execute: {
                        ImageCache.shared.save(image: downloadedImage, forKey: urlString)
                        if self.imageUrlString == urlString {
                            self.image = downloadedImage
                        }
                    })
                }
                
            }
            currentTask = dataTask
            dataTask.resume()
        }
    }
}
