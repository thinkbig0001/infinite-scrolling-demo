//
//  RedditDataModel.swift
//  InfiniteScrollingDemo
//
//  Created by TAPAN BISWAS on 7/25/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import Foundation


struct Record : Codable {
    var title: String
    var thumbnail: String?      //Contains the URL, if any
    
    enum CodingKeys: String, CodingKey {
        case title
        case thumbnail
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: CodingKeys.title)
        thumbnail = try values.decodeIfPresent(String.self, forKey: CodingKeys.thumbnail)
    }
}

struct ChildRecord : Codable {
    var kind: String
    var data: Record?
    
    enum CodingKeys: String, CodingKey {
        case kind
        case data
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        kind = try values.decode(String.self, forKey: CodingKeys.kind)
        data = try values.decodeIfPresent(Record.self, forKey: CodingKeys.data)
    }

}

struct MainRecord : Codable {
    var modhash: String?
    var dist: Int?
    var children: [ChildRecord]?
    var after: String?
    var before: String?
    
    enum CodingKeys: String, CodingKey {
        case modhash
        case dist
        case children
        case after
        case before
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        modhash = try values.decodeIfPresent(String.self, forKey: CodingKeys.modhash)
        dist = try values.decodeIfPresent(Int.self, forKey: CodingKeys.dist)
        children = try values.decodeIfPresent([ChildRecord].self, forKey: CodingKeys.children)
        after = try values.decodeIfPresent(String.self, forKey: CodingKeys.after)
        before = try values.decodeIfPresent(String.self, forKey: CodingKeys.before)
    }

}

struct CoreModel : Codable {
    var kind: String
    var data: MainRecord?
    
    enum CodingKeys: String, CodingKey {
        case kind
        case data
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        kind = try values.decode(String.self, forKey: CodingKeys.kind)
        data = try values.decodeIfPresent(MainRecord.self, forKey: CodingKeys.data)
    }
}

final class RedditDataModel {
    var isLoading: Bool = false
    
    var records: [Record] = []
    var main: CoreModel?  {
        didSet {
            main?.data?.children?.forEach({ (rec) in
                records.append(rec.data!)
            })
        }
    }
    let baseURL = "http://www.reddit.com/.json"
        
    func getNextURL() -> String? {
        return self.main?.data?.after
    }
    
    func loadDataFromRemote(completionHandler: @escaping (Bool)->Void) {
        var finalURL = baseURL
        
        //Don't allow multiple calls when dataload is still in progress
        if isLoading {
            return
        }
        
        //Add the next URL query criteria, if present
        if let nextURL = getNextURL() {
            finalURL += "?after=" + nextURL
        }
        
        guard let url = URL(string: finalURL) else {
            print("Error: Error trying to convert URL endpoint")
            isLoading = false
            return
        }
        
        //Basic standard URL Session setup
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 10
        session.configuration.waitsForConnectivity = false
        
        //Make the data request and handle the callback
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil else {
                print("Error: Unable to retrive remote data for school list \n\(error?.localizedDescription ?? "")")
                self.isLoading = false
                return
            }
            if let resp = response as? HTTPURLResponse {
                if resp.statusCode  != 200 {
                    print("Error: Response Code: \(resp.statusCode) :: \(resp.debugDescription)")
                    self.isLoading = false
                    return
                }
            }
            guard let returnedData = data else {
                print("No data received")
                self.isLoading = false
                return
            }
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(CoreModel.self, from: returnedData)
                self.main = decoded
                self.isLoading = false
                completionHandler(true)
            } catch  {
                print("Error: Error trying to convert data to readable format")
                self.isLoading = false
                return
            }
        }
        isLoading = true
        task.resume()
        
    }

}
