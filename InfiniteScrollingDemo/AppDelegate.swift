//
//  AppDelegate.swift
//  InfiniteScrollingDemo
//
//  Created by TAPAN BISWAS on 7/25/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

